<?php

session_start();

require "vendor/autoload.php";
require "LinkedIn.php";

use myPHPnotes\LinkedIn;

$app_id = "784oi4h55b90bw";
$app_secret = "CaApYToJWudxsAIB";
$app_callback = "http://localhost/linkedin_api/callback.php";
$app_scopes = "r_emailaddress r_liteprofile w_member_social";

$ssl = false;

$linkedin = new LinkedIn($app_id, $app_secret, $app_callback, $app_scopes, $ssl);